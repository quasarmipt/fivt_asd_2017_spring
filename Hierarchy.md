##Структура каталогов

Оформите следующую структуру каталогов в репозитории:
```
.
├── CMakeLists.txt  # Корневой cmake-list
├── .gitignore      # подсказки git
├── task-1-A        # Каталог задачи A контеста 1
│   ├── CMakeLists.txt  # cmake-list для сборки этой задачи
│   └── solution.cpp    # файл или файлы этой задачи
└── task-1-B   # Задача B контеста 1
    ├── CMakeLists.txt
    └── solution.cpp
```

Подобная структура позволит вам удобно работать над разными задачими.
Изменения в рамках одной задачи локализуются в одной папке, поэтому
при работе с одной задачей вам будет сложно сделать изменения,
которые затронут файлы другой задачи. Это очень важно в момент
создания Pull Request в bitbucket, при конфликтующих изменениях
вам нужно будет вручную убирать конфликты.


## Корневой CMakeList.txt
Минимальный вариант:
```
cmake_minimum_required(VERSION 3.6)
project(Fivt) 
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
add_subdirectory(task-1-A)
add_subdirectory(task-1-B)
```

```project(Fivt)``` -- задаёт название проекта. Можно написать что угодно,
нужно один раз в корневом листе.

```set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")``` -- включаем опцию компиляции cpp-14

```add_subdirectory(task-1-A)``` -- добавляем в сборку папку с задачей

Корневой cmake-list позволит собрать весь ваш проект целиком.

Для пользователей linux можно ускорить сборку через кеширование
результатов компиляции файлов. Это можно сделать добавив
следующий фрагмент в корневой CMakeLists.txt

```cmake
# Ускоряем компиляцию через ccache, если он есть
find_program(CCACHE_PROGRAM ccache)
if(CCACHE_PROGRAM)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "${CCACHE_PROGRAM}")
    set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK "${CCACHE_PROGRAM}")
    set(GLOBAL_CMAKE_CCACHE_RULES -DCMAKE_C_COMPILER=/usr/lib/ccache/cc -DCMAKE_CXX_COMPILER=/usr/lib/ccache/c++)
    set(GLOBAL_CCACHE_ENV -DCMAKE_C_COMPILER=/usr/lib/ccache/cc -DCMAKE_CXX_COMPILER=/usr/lib/ccache/c++)
endif(CCACHE_PROGRAM)
```

##CMakeLists.txt папки с задачей

Базовый вариант:

```
add_executable(solution-1-a solution.cpp)
```

сигнатура команды add_executable:
```
add_executable(<имя компилируемого бинарного файла> исходник1.cpp исходник2.cpp ...
```


## Результат картинкой
![Hierarchy](images/project_structure.png)
