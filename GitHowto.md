[TOC]

##Документация
Русскоязычная документация git [https://git-scm.com/book/ru/v1](https://git-scm.com/book/ru/v1)


## Мой .gitconfig 

```ini
[user]
    name = !!Ваше имя!!
    email = !!Ваша почта!!
[push]
    default = upstream
[alias]
    co = checkout
    cm = commit
    st = status
    br = branch
    df = diff
    hist = log --oneline --abbrev-commit --all --graph --decorate --color
    ci = commit
    # View the current working tree status using the short format
    s = status -s
    # Show the diff between the latest commit and the current state
    d = !"git diff-index --quiet HEAD -- || clear; git --no-pager diff --patch-with-stat"
    # `git di $number` shows the diff between the state `$number` revisions ago and the current state
    di = !"d() { git diff --patch-with-stat HEAD~$1; }; git diff-index --quiet HEAD -- || clear; d"
    # Switch to a branch, creating it if necessary
    go = checkout -B
    # Show verbose output about tags, branches or remotes
    tags = tag -l
    branches = branch -a
    remotes = remote -v
    lm = log --pretty=format:'%Cred%h%Creset %C(bold blue)<%an>%Creset \
      -%C(yellow)%d%Creset %C(bold cyan)%s %Cgreen(%cr)%n%Creset%n - %b%n' \
      --abbrev-commit --date=relative --merges

[color]
    # Use colors in Git commands that are capable of colored output when
    # outputting to the terminal. (This is the default setting in Git � 1.8.4.)
    ui = auto
    diff = auto
    status = auto
    branch = auto
[color "branch"]
    current = yellow reverse
    local = yellow
    remote = green
[color "diff"]
    meta = yellow bold
    frag = magenta bold
    old = red bold
    new = green bold
[color "status"]
    added = yellow
    changed = green
    untracked = cyan
[merge]
    # Include summaries of merged commits in newly created merge commit messages
    log = true

# URL shorthands
# 
# For example, if you find the following command too long:
# git clone git://git.kde.org/amarok
# 
# Then you can define a URL alias by putting the following into the file .gitconfig in your home directory:
# 
# [url "git://git.kde.org/"]
#     insteadOf = kde://
# 
# Now the command shortens considerably:
# 
# git clone kde://amarok
[url "git@github.com:"]
        insteadOf = "gh:"
[url "git@gist.github.com:"]
        insteadOf = "gist:"
[url "git@bitbucket.org"]
        insteadOf = "bb:"

```