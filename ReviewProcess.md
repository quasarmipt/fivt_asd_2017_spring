[TOC]

##Отведение бранча и реализация программы
Исходим из предположения, что вы живёте в рекомендуемой структуре каталогов, как описано [здесь](Hierarchy.md)


Смотрим в каком бранче мы находимся. Активный бранч помечен `*`
```
~/fivt$ git branch    
* master
~/fivt$ git status
On branch master
Your branch is up-to-date with 'origin/master'.
nothing to commit, working tree clean
~/fivt$
```

Отводить ветку мы будем от master, важно находится в свежем мастере.

<!--
```
~/fivt$ git hist
* dd37c83 (HEAD -> master, origin/master) added task 1-B
* bc5b15f added task 1-A
* e971993 added root cmake list
* 866507e Initial commit
```
-->

Состояние репозитория в момент начала работы:
```
~/fivt$ ls
CMakeLists.txt  task-1-A  task-1-B
```

Отводим ветку:

```
~/fivt$ git checkout -b task-1-C
Switched to a new branch 'task-1-C'

~/fivt$ git branch
  master
* task-1-C

~/fivt$
```

создаём каталог task-1-C, создаём в нём файл решения.
В конревой CMakeList.txt добавляем новую директорию и создаём 
CMakeLists.txt в каталоге task-1-C

просим git начать следить за нашими новыми файлами.

```
~/fivt$ git add task-1-C/CMakeLists.txt task-1-C/main.cpp

<редактируем файлы>

~/fivt/task-1-C$ git status
On branch task-1-C
Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

        new file:   CMakeLists.txt
        new file:   main.cpp

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   ../CMakeLists.txt
        modified:   CMakeLists.txt
        modified:   main.cpp       
```
```diff
~/fivt$ git diff
diff --git a/CMakeLists.txt b/CMakeLists.txt
index 92220da..105234d 100644
diff --git a/CMakeLists.txt b/CMakeLists.txt
index 92220da..105234d 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -2,4 +2,5 @@ cmake_minimum_required(VERSION 3.6)
 project(Fivt)
 set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
 add_subdirectory(task-1-A)
-add_subdirectory(task-1-B)
\ No newline at end of file
+add_subdirectory(task-1-B)
+add_subdirectory(task-1-C)
diff --git a/task-1-C/CMakeLists.txt b/task-1-C/CMakeLists.txt
new file mode 100644
index 0000000..6c092d9
--- /dev/null
+++ b/task-1-C/CMakeLists.txt
@@ -0,0 +1 @@
+add_executable(task-1-c main.cpp)
diff --git a/task-1-C/main.cpp b/task-1-C/main.cpp
new file mode 100644
index 0000000..cf21405
--- /dev/null
+++ b/task-1-C/main.cpp
@@ -0,0 +1,5 @@
+#include<iostream>
+
+int main() {
+       std::cout << "C solution will soon appear here!\n";
+}

```

занимаемся реализацией решения, в поцессе перидически коммитим изменения.

```
~/fivt$ git commit -m 'working on task-1-C'                         
[task-1-C 4128d04] working on task-1-C
 3 files changed, 8 insertions(+), 1 deletion(-)
 create mode 100644 task-1-C/CMakeLists.txt
 create mode 100644 task-1-C/main.cpp   
~/fivt$ git push
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 449 bytes | 0 bytes/s, done.
Total 4 (delta 1), reused 0 (delta 0)
remote:
remote: Create pull request for task-1-C:
remote:   https://bitbucket.org/misty_fungus/fivt/pull-requests/new?source=task-1-C&t=1
remote:
To bitbucket.org:misty_fungus/fivt.git
   4128d04..52b81b2  task-1-C -> task-1-C                          
```

##Сдача домашней работы в yandex.contest

Можно почитать документацию сервиса по [ссылке](https://yandex.ru/support/contest/index.html)

##Создание пулл реквеста и прохождение ревью

Состояние репозитория и кнопка создания PR:
![Create_PR](images/create_pr.png)

        
Форму пулл реквеста заполняем вот так:
![PR form](images/create_pr_form.png)

*ВАЖНО*: оформляйте форму как на картинке

Результат должен выглядеть как [в этом примере](https://bitbucket.org/misty_fungus/fivt/pull-requests/3/1/diff)



##Скринкаст: VisualStudio + bitbucket
TBD

##Скринкаст: CLion + bitbucket
[![Video](http://img.youtube.com/vi/bAtRwvsu71Y/0.jpg)](https://www.youtube.com/watch?v=bAtRwvsu71Y)